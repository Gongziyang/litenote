package com.litenote.mynote.util;

import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/8 15:19
 * @UpdateDate: 2022/3/8 15:19
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class MyBloomFilter {

    /**
     * 布隆过滤器长度
     */
    private int size = 1000000;

    /**
     * 误判率
     */
    private float fpp = 0.01f;

    private BloomFilter<Integer> bloomFilter = BloomFilter.create(Funnels.integerFunnel(), size, fpp);

    private int count;

    public static void main(String[] args) {
        MyBloomFilter filter = new MyBloomFilter();
        for (int i = 0; i < 1000000; i++) {
            filter.bloomFilter.put(i);
        }

        for (int i = 1000000; i < 2000000; i++) {
            if (filter.bloomFilter.mightContain(i)) {
                filter.count++;
            }
        }
        System.out.println("总共发生：" + filter.count + "次hash碰撞");
    }
}
