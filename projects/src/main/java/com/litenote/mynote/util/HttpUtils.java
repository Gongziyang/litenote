package com.litenote.mynote.util;

import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

/**
 * @author: yyf
 * @create: 2021 04 2021/4/6 8:39
 * @file: HttpUtils
 * @description: 星期二
 */
public class HttpUtils {

    Logger logger = LoggerFactory.getLogger(HttpUtils.class);

    static RestTemplate reqTemplate = new RestTemplate();

    /**
     * @param url    : 请求地址
     * @param params : 请求参数
     *               description : 请求平台认证，获取token
     **/
    public static String doPostReq(String url, Map<String, String> params) {
        String postStr = JSONObject.toJSONString(params);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=utf-8");
        HttpEntity<String> postEntity = new HttpEntity<>(postStr, httpHeaders);
        ResponseEntity<String> stringResponseEntity = reqTemplate.postForEntity(url, postEntity, String.class);
        //请求成功
        if (stringResponseEntity.getStatusCode() == HttpStatus.OK) {
            System.out.println("[response-body]\t" + stringResponseEntity.getBody());
            return stringResponseEntity.getBody();
        } else {
            return null;
        }
    }

    /**
     * @param url    : 请求地址
     * @param params ： 请求参数
     * @param token  ：token凭证
     *               description: 用于回调第三方平台的接口
     **/
    public static Object doPostReq(String url, Map<String, String> params, String token) {
        String postStr = JSONObject.toJSONString(params);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=utf-8");
        //请求头中设置token
        httpHeaders.add("Authorization", "Bearer " + token);
        HttpEntity<Object> httpEntity = new HttpEntity<>(postStr, httpHeaders);
        ResponseEntity<Object> objectResponseEntity = reqTemplate.postForEntity(url, httpEntity, Object.class);
        if (objectResponseEntity.getStatusCode() == HttpStatus.OK) {
            return objectResponseEntity.getBody();
        } else {
            return null;
        }
    }

    public static String doGetReq(String url, Map<String, Object> params) {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json;charset=utf-8");
        ResponseEntity<String> forEntity = reqTemplate.getForEntity(url, String.class, params);
        if (forEntity.getStatusCode().equals(HttpStatus.OK)) {
            return forEntity.getBody();
        } else {
            return null;
        }
    }
}
