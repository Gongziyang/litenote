package com.litenote.mynote.util;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/12 19:55
 * @UpdateDate: 2022/2/12 19:55
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 主键生成器
 */
@Component
public class IdWorker implements IdentifierGenerator {

    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    private static Integer NUMBER_BOUND = 9999;
    private static int ID_LENGTH = 16;

    @Override
    public Number nextId(Object entity) {
        int i = ThreadLocalRandom.current().nextInt(NUMBER_BOUND);
        String timeStr = String.valueOf(System.currentTimeMillis());
        timeStr = timeStr.substring(5);
        if (atomicInteger.get() == NUMBER_BOUND) {
            atomicInteger.set(0);
        }
        int i1 = atomicInteger.getAndIncrement();
        String id = timeStr.concat(String.valueOf(i).concat(i1 + ""));
        if (id.length() > ID_LENGTH) {
            id = id.substring(id.length() - ID_LENGTH);
        }
        return Long.valueOf(id);
    }


}
