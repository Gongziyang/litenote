package com.litenote.mynote.util;

import cn.hutool.core.util.ObjectUtil;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/16 23:24
 * @UpdateDate: 2022/2/16 23:24
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu param check utils
 */
public class ParamsCheckUtils {

    public static boolean hasEmpty(Object... params) {
        if (ObjectUtil.isNull(params)) {
            return Boolean.FALSE;
        }
        for (int i = 0; i < params.length; i++) {
            if (ObjectUtil.isNull(params[i])) {
                return Boolean.FALSE;
            }
        }
        return Boolean.TRUE;
    }
}
