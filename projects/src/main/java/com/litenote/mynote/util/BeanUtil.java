package com.litenote.mynote.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/12 18:37
 * @UpdateDate: 2022/2/12 18:37
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu spring实例工具类
 */
@Component
public class BeanUtil implements ApplicationContextAware {

    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        context = applicationContext;
    }

    public static <T> T getBeanByClass(Class<T> tClass) {
        return context.getBean(tClass);
    }

    public static Object getBeanByName(String name) {
        return context.getBean(name);
    }

    public static <T> T getBean(Class<T> tClass, String name) {
        return context.getBean(name, tClass);
    }
}
