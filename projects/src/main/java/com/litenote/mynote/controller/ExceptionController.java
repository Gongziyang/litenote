package com.litenote.mynote.controller;

import com.litenote.mynote.exceptions.BizException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/21 10:56
 * @UpdateDate: 2022/2/21 10:56
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@RestController
@RequestMapping("/exception_handle/")
public class ExceptionController {

    @RequestMapping("catch")
    public void exceptionHandle(ServletRequest servletRequest) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        throw (BizException) request.getAttribute("filter.error");
    }
}
