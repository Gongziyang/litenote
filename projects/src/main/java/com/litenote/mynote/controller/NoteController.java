package com.litenote.mynote.controller;

import com.litenote.mynote.qo.*;
import com.litenote.mynote.vo.*;
import com.litenote.mynote.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.DispatcherServlet;

import java.util.List;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:22
 * @UpdateDate: 2022/2/11 23:22
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 笔记控制器
 */
@RestController
@RequestMapping("/note/")
@Api("笔记管理")
public class NoteController {

    @Autowired
    CommentInfoService commentInfoService;

    @Autowired
    NoteDetailService noteDetailService;

    @Autowired
    NoteService noteService;

    @Autowired
    UserInfoService userInfoService;


    /**
     * @Description: 分页查询笔记列表
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("note_by_page")
    @ApiOperation("分页查询笔记信息")
    public BaseVo<List<NoteVo>> notesByPage(@RequestBody NotesQo noteQo) {
        return noteService.notesByPage(noteQo);
    }

    /**
     * @Description: 新增笔记
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("save_note")
    @ApiOperation("保存/编辑笔记")
    public BaseVo saveNote(@RequestBody SaveNoteQo saveNoteQo) {
        return noteService.saveNote(saveNoteQo);
    }

    /**
     * @Description: 删除笔记
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @GetMapping("delete_note")
    @ApiOperation("删除笔记")
    public BaseVo deleteNote(@RequestParam("noteId") Long noteId
            , @RequestParam("userId") Long userId) {
        return noteService.deleteNote(noteId,userId);
    }

    /**
     * @Description: 查看笔记评论列表
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("query_comment_list")
    @ApiOperation("查询评论列表")
    public BaseVo<List<CommentVo>> queryCommentList(@RequestBody QueryCommentQo queryCommentQo) {
        return commentInfoService.queryCommentList(queryCommentQo);
    }

    /**
     * @Description: 查看笔记详情
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @GetMapping("note_detail")
    @ApiOperation("查看笔记详情（评论数量、点赞数量）")
    public NoteDetailVo noteDetail(@RequestParam("noteId") Long noteId
            , @RequestParam(value = "userId", required = false) Long userId) {
        return noteDetailService.noteDetail(noteId, userId);
    }

    /**
     * @Description: 获取上一条，下一条
     * @Param: noteId 笔记列表请求体
     * @Param: index 0上一条 1下一条
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @GetMapping("note_pre_or_next")
    @ApiOperation("查看上一条下一条笔记详情")
    public NoteDetailVo getPreOrNext(@RequestParam("noteId") Long noteId,
                                     @RequestParam("index") Integer index,
                                     @RequestParam(value = "userId", required = false) Long userId) {
        return noteDetailService.getPreOrNext(noteId, index, userId);
    }

    /**
     * @Description: 点赞
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("apply_note")
    @ApiOperation("点赞")
    public void applyNote(@RequestBody @Validated ApplyQo qo) {
        noteService.applyNote(qo);
    }

    /**
     * @Description: 取消点赞
     * @Param: qo 取消点赞请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("un_apply_note")
    @ApiOperation("取消点赞")
    @Transactional(rollbackFor = Exception.class)
    public void unApplyNote(@RequestBody @Validated ApplyQo qo) {
        noteService.unApplyNote(qo);
    }

    /**
     * @Description: 评论
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("comment_note")
    @ApiOperation("评论")
    public BaseVo commentNote(@RequestBody CommentQo commentQo) {
        return noteService.commentNote(commentQo);
    }


    /**
     * @Description: 删除评论
     * @Param: commentId 笔记ID
     * @Param: userId 用户ID
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @GetMapping("delete_comment")
    @ApiOperation("删除评论")
    public BaseVo deleteComment(@RequestParam("commentId") Long commentId,
                                @RequestParam("userId") Long userId) {
        return noteService.deleteComment(commentId, userId);
    }

    /**
     * @Description: 获取点赞用户列表
     * @Param: commentId 笔记ID
     * @Param: userId 用户ID
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("get_apply_users")
    @ApiOperation("查看点赞用户列表")
    public BaseVo queryApplyUsers(@RequestBody QueryApplyQo queryApplyQo) {
        return noteService.queryApplyUsers(queryApplyQo);
    }
}
