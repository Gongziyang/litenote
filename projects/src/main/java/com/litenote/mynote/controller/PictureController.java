package com.litenote.mynote.controller;

import com.litenote.mynote.qo.SavePicInfoQo;
import com.litenote.mynote.service.PictureService;
import com.litenote.mynote.vo.BaseVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/17 22:00
 * @UpdateDate: 2022/2/17 22:00
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@RestController
@RequestMapping("/note/picture/")
@Api("图片控制器")
public class PictureController {

    @Autowired
    PictureService pictureService;

    /**
     * @Description: 上传图片接口
     * @Param:
     * @Param:
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return:
     * @date: 2022/2/17 22:27
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("save_pic")
    @ApiOperation("保存/编辑 图片")
    public BaseVo savePicture(@RequestParam("file") MultipartFile file,
                              @RequestParam(value = "noteId", required = false) Long noteId) {
        return pictureService.savePicture(file, noteId);
    }

    /**
     * @Description: 编辑图片说明
     * @Param: savePicInfoQo 请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return:
     * @date: 2022/2/17 22:27
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("save_pic_info")
    @ApiOperation("保存/编辑 图片")
    public BaseVo savePictureInfo(@RequestBody SavePicInfoQo savePicInfoQo) {
        return pictureService.savePictureInfo(savePicInfoQo);
    }

    /**
     * @Description: 删除图片
     * @Param: savePicInfoQo 请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return:
     * @date: 2022/2/17 22:27
     * @version: 1.0
     * @status: undone
     */
    @GetMapping("delete_picture")
    @ApiOperation("保存图片 说明")
    public BaseVo deletePicture(@RequestParam("pictureId") Long pictureId) {
        return pictureService.deletePicture(pictureId);
    }

    /**
     * @Description: 获取图片Url
     * @Param: savePicInfoQo 请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return:
     * @date: 2022/2/17 22:27
     * @version: 1.0
     * @status: undone
     */
    @GetMapping("pic_urls")
    @ApiOperation("获取图片urls")
    public BaseVo getPictureUrls(@RequestParam("noteId") Long noteId) {
        return pictureService.getPictureUrls(noteId);
    }
}
