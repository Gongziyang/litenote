package com.litenote.mynote.controller;

import com.litenote.mynote.qo.wx.qo.AuthQo;
import com.litenote.mynote.qo.wx.vo.AuthVo;
import com.litenote.mynote.service.UserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:22
 * @UpdateDate: 2022/2/11 23:22
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 用户信息控制器
 */
@RestController
@RequestMapping("/note/user/")
@Api("用户控制器")
public class UserController {


    @Autowired
    UserInfoService userInfoService;

    /**
     * @Description: 获取用户session_key
     * @Param: authQo 请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @PostMapping("login")
    @ApiOperation("获取session_key")
    public AuthVo code2Session(@RequestBody AuthQo authQo) {
        return userInfoService.code2Session(authQo);
    }
}
