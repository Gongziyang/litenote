package com.litenote.mynote.es.utils;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.es.pojo.DocMappingModel;
import com.litenote.mynote.exceptions.BizException;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.get.GetIndexRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/14 16:49
 * @UpdateDate: 2022/3/14 16:49
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Slf4j
@Component
public class ESUtil {

    public static ExecutorService executorService = Executors.newFixedThreadPool(3);

    @Autowired
    RestHighLevelClient restHighLevelClient;

    @Value("${elasticsearch.shards}")
    Integer shards;

    @Value("${elasticsearch.replicas}")
    Integer replicas;

    /**
     * 创建索引 同时创建一个type
     * post http://localhost/{index}/{type}/_mapping
     * {
     *     "settings":{
     *         "number_of_shards":"2",
     *         "number_of_replicas":"1"
     *     },
     *     "properties":{
     *         "description":{
     *             "type":"text",
     *             "analyzer":"ik_max_word",
     *             "search_analyzer":"ik_smart"
     *         },
     *         "name":{
     *             "type":"text",
     *              "analyzer":"ik_max_word",
     *              "search_analyzer":"ik_smart"
     *         },
     *         "pic":{ // 图片地址
     *             "type":"text",
     *             "index":false // 地址不用来搜索，因此不为它构建索引
     *         },
     *         "price": { // 价格
     *         	"type":"scaled_float", // 有比例浮点
     *          "scaling_factor":100 // 比例因子 100
     *         },
     *         "studymodel": {
     *             "type": "keyword" // 不分词，全关键字匹配（精确匹配）
     *         },
     *         "timestamp": {
     *             "type": "date",
     *             "format": "yyyy-MM-dd HH:mm:ss||yyyy-MM-dd||epoch_millis"
     *         }
     *     }
     * }
     */
    public void createIndex(String index) {
        if (StrUtil.isEmpty(index)) {
            throw new BizException(Bizerror.ES_NAME_ISEMPTY);
        }
        CreateIndexResponse createIndexResponse;
        CreateIndexRequest createIndexRequest = new CreateIndexRequest(index);
        createIndexRequest.settings(Settings.builder()
                .put("number_of_shards", shards)
                .put("number_of_replicas", replicas));
        try {
            createIndexResponse = restHighLevelClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("创建索引失败，失败信息：{}", JSONObject.toJSONString(createIndexRequest));
            throw new RuntimeException("船舰索引失败！");
        }
        //是否响应
        boolean acknowledged = createIndexResponse.isAcknowledged();
        if (acknowledged) {
            log.info("创建索引成功！");
        }
    }

    /**
     * 判断索引是否存在
     */
    public boolean ifExist(String index) {
        GetIndexRequest getIndexRequest = new GetIndexRequest();
        getIndexRequest.indices(index);
        boolean exists;
        try {
            exists = restHighLevelClient.indices().exists(getIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("查询失败！索引：{}", index);
            return false;
        }
        return exists;
    }

    /**
     * 删除索引
     */
    public boolean delIndex(String... indices) {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(indices);
        AcknowledgedResponse acknowledgedResponse = null;
        try {
            acknowledgedResponse = restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.info("删除索引失败，索引：{}", indices);
        }
        return ObjectUtil.isNotNull(acknowledgedResponse) && acknowledgedResponse.isAcknowledged();
    }

    /**
     * 全量搜索，类似于select *
     */
    public List<DocMappingModel> query(String type, String[] fieldSources, String... indices) {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.types(type);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.fetchSource(fieldSources, new String[]{});
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = null;
        try {
            searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.info("查询失败：index:{},type:{}", indices, type);
        }
        return parseResult(searchResponse);
    }

    /**
     * 分页全量搜索，类似于select *
     * post http://localhost:9200/{index}/{type}/_search
     * {
     *     "query":{
     *         "match_all":{}
     *     },
     *     "_source":["name","studymodel"],
     *     "from": 1,
     *     "size": 1
     * }
     */
    public List<DocMappingModel> queryByPage(String type, Integer from, Integer size, String... indices) {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.types(type);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.from((from - 1) * size);
        searchSourceBuilder.size(size);
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = null;
        try {
            search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.info("查询失败，index:{},type:{}", indices, type);
        }
        return parseResult(search);
    }

    /**
     * 精确检索，关键词不会被分词
     * post http://localhost:9200/{index}/{type}/_search
     * {
     *     "query":{
     *         "term":{
     *             "name":"中国四川"
     *         }
     *     },
     *     "_source":["name","studymodel"]
     * }
     * 或者
     * get http://localhost:9200/{index}/{type}/1  : 按ID查询，该查询也是精确查询
     * 通过searchSourceBuilder.query(QueryBuilders.termQuery("_id","1"))
     * 或者批量ID：searchSourceBuilder.query(QueryBuilders.termsQuery("_id","1","2"))
     *
     */
    public List<DocMappingModel> termQuery(String type, String field, String keyWord, String[] fieldSources, String... indices) {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.types(type);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.fetchSource(fieldSources, new String[]{});
        searchSourceBuilder.query(QueryBuilders.termQuery(field, keyWord));
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = null;
        try {
            searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.info("查询失败，index：{}，type：{}，keyWord：{}", indices, type, keyWord);
        }
        return parseResult(searchResponse);
    }


    /**
     * 全文搜素，关键词会被分词，例如【中国四川】 会被分为：【中国】 【四川】两个词，然后再去搜索
     * post http://localhost:9200/{index}/{type}/_search
     * {
     *      "query":{
     *          "match":{
     *              "name":{
     *                  "query":"中国四川",
     *                  "operator":"OR",
     *                  "minimum_should_match":"70%"
     *              }
     *          }
     *      },
     *      "_source":["name","studymodel"]
     * }
     */
    public List<DocMappingModel> matchQuery(String type, String field, String keyWord, String[] sourceFields, String... indices) {
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.types(type);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery(field, keyWord)
                .operator(Operator.OR).minimumShouldMatch("70%"));
        searchSourceBuilder.fetchSource(sourceFields,new String[]{});
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = null;
        try {
            searchResponse = restHighLevelClient.search(searchRequest,RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.info("查询失败，index：{}，type：{}，keyWord：{}", indices, type, keyWord);
        }
        return parseResult(searchResponse);
    }

    /**
     * 多字段联合查询，当不知道用户想搜索的是哪一个field的时候，可以用multi_match
     * post http://localhost/{index}/{type}/_search
     * {
     *     "query":{
     *         "multi_match":{
     *              "query":"中国四川",
     *              "minimum_should_match":"70%",
     *              "fields":["name","studymodel"],
     *              "operator":"OR"
     *         }
     *     },
     *     "_source":["name","description"]
     * }
     */
    public List<DocMappingModel> multiQuery(String type, String[] fields, String keyWord, String[] fieldSources, String... indices){
            SearchRequest searchRequest = new SearchRequest(indices);
            searchRequest.types(type);
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.multiMatchQuery(keyWord,fields)
                    .minimumShouldMatch("70%")
                    .field("name",10)
                    .operator(Operator.OR));
            searchSourceBuilder.fetchSource(fieldSources,new String[]{});
            SearchResponse searchResponse = null;
            try{
                searchResponse = restHighLevelClient.search(searchRequest,RequestOptions.DEFAULT);
            }catch (IOException e){
                log.info("查询失败，index：{}，type：{}",indices,type);
            }
            return parseResult(searchResponse);
    }

    /**
     * bool搜索 当搜索条件不仅仅满足于 match、multi_match、term中的任意一个而是多个组合时，可使用bool搜索
     * must: 满足查询条件的交集（and）
     * should: 满足查询条件的并集（or）
     * must_not: 不满足全部查询条件
     * post http://localhost:9200/{index}/{type}/_search
     * {
     *     "query":{
     *         "bool":{
     *             "must":{
     *                 "multi_match":{
     *                     "query":"中国四川",
     *                     "minimum_should_match":"70%",
     *                     "operator":"OR",
     *                     "fields":["name^10","studymodel^20"]
     *                 },
     *                 "term":{
     *                     "name":“中国”
     *                 }
     *             },
     *             #添加过滤条件
     *             "filter":[
     *                 "term":{
     *                     "studymodel":"202001"
     *                 },
     *                 "range":{
     *                     "price":{"gte":60,"lte":100}
     *                 }
     *             ]
     *         }
     *     }，
     *     “_source”:["name","studeymodel"]
     * }
     */
    public List<DocMappingModel> boolQuery(String type,
                                           String[] mFields,
                                           String mKeyWord,
                                           String tField,
                                           String tKeyWord,
                                           String[] fieldSources, String... indices){
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.types(type);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.fetchSource(fieldSources,new String[]{});
        MultiMatchQueryBuilder multiQuery = QueryBuilders.multiMatchQuery(mKeyWord, mFields)
                .operator(Operator.OR)
                .minimumShouldMatch("70%")
                .field(mFields[0], 10)
                .field(mFields[1], 20);
        TermQueryBuilder termQuery = QueryBuilders.termQuery(tField, tKeyWord);
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        boolQueryBuilder.must(multiQuery);
        boolQueryBuilder.must(termQuery);

        //过滤实现
//        boolQueryBuilder.filter(QueryBuilders.termQuery("studymodel","202001"));
//        boolQueryBuilder.filter(QueryBuilders.rangeQuery("price").gte(60).lte(100));

        searchSourceBuilder.query(boolQueryBuilder);
        SearchResponse searchResponse = null;
        try {
            searchResponse =  restHighLevelClient.search(searchRequest,RequestOptions.DEFAULT);
        }catch (IOException e){
            log.info("查询失败! index：{}，type：{}",indices,type);
        }
        return parseResult(searchResponse);
    }

    /**
     *  排序
     *  post http://localhost:9200/index{}/{type}/_search
     *  {
     *      "query":{
     *          ...
     *      },
     *      "sort":[
     *          {"name":"desc"},
     *          {"price":"asc"}
     *      ]
     *  }
     */
    public List<DocMappingModel> sorted(String type, String[] fieldSources, String... indices){
        SearchRequest searchRequest = new SearchRequest(indices);
        searchRequest.types(type);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.fetchSource(fieldSources,new String[]{});
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        //第一降序规则
        searchSourceBuilder.sort("name", SortOrder.DESC);
        //第二升序规则
        searchSourceBuilder.sort("studymodel",SortOrder.ASC);
        searchRequest.source(searchSourceBuilder);
        SearchResponse searchResponse = null;
        try {
            searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        }catch (IOException e){
            log.info("查询失败，index:{},type:{}",indices,type);
        }
        return parseResult(searchResponse);
    }
    /**
     * 结果转换
     * @param searchResponse 搜索结果
     */
    public List<DocMappingModel> parseResult(SearchResponse searchResponse) {
        if(ObjectUtil.isNull(searchResponse)){
            return Collections.emptyList();
        }
        SearchHits hits = searchResponse.getHits();
        log.info("查询到：{}条记录", hits.totalHits);
        return Arrays.stream(hits.getHits()).map(h -> {
            DocMappingModel docMappingModel;
            docMappingModel = JSONObject.parseObject(h.getSourceAsString(), DocMappingModel.class);
            docMappingModel.setId(h.getId());
            return docMappingModel;
        }).collect(Collectors.toList());
    }

    /**
     * 创建document
     * @param index 索引
     * @param type 类型
     * @param models 数据
     */
    public void createDocument(String index,String type,List<DocMappingModel> models){
        IndexRequest indexRequest = new IndexRequest(index,type);
        indexRequest.source(models);
        executorService.execute(() ->{
            try {
                restHighLevelClient.index(indexRequest,RequestOptions.DEFAULT);
            }catch (IOException e){
                log.info("新增失败!index:{},type:{}",index,type);
            }
        });
    }

    /**
     * 删除记录
     *
     */
    public void deleteRecord(String index,String type,String id){
        DeleteRequest deleteRequest = new DeleteRequest(index,type,id);
        try {
            restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
        }catch (IOException e){
            log.info("删除失败!index:{},type:{},id:{}",index,type,id);
        }
    }

    /**
     * 更新记录
     */
    public void updateRecord(String index,String type,String id){
        UpdateRequest updateRequest = new UpdateRequest(index,type,id);
        try {
            restHighLevelClient.update(updateRequest,RequestOptions.DEFAULT);
        }catch (IOException e){
            log.info("更新失败！index：{}，type：{}，id：{}",index,type,id);
        }
    }
}
