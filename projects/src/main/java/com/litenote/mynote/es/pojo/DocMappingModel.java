package com.litenote.mynote.es.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/14 16:40
 * @UpdateDate: 2022/3/14 16:40
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
@ToString
@NoArgsConstructor
public class DocMappingModel {

    private String id;
    private String name;
    private String description;
    String studymodel;
    Double price;
    Date timestamp;
    String pic;
}
