package com.litenote.mynote.es.config;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/14 16:45
 * @UpdateDate: 2022/3/14 16:45
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Slf4j
@Configuration
public class EsConfig {

    @Value("${elasticsearch.ip}")
    String host;

    @Value("${elasticsearch.port}")
    Integer port;

    @Bean
    RestHighLevelClient restHighLevelClient() {
        String[] hosts = host.trim().split(",");
        HttpHost[] httpHosts = new HttpHost[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            httpHosts[i] = new HttpHost(hosts[i], port, "http");
        }
        return new RestHighLevelClient(RestClient.builder(httpHosts));
    }

}
