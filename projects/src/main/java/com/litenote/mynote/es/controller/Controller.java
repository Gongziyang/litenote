package com.litenote.mynote.es.controller;

import com.litenote.mynote.es.pojo.DocMappingModel;
import com.litenote.mynote.es.utils.ESUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/14 16:53
 * @UpdateDate: 2022/3/14 16:53
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@RestController
@RequestMapping("/es/")
public class Controller {

    @Autowired
    ESUtil esUtil;

    @RequestMapping("search")
    public List<DocMappingModel> query(@RequestParam("type") String type, @RequestParam("index") String... indices) {
        return esUtil.query(type,new String[]{"name","studymodel"},indices);
    }

    @RequestMapping("create")
    public void create(@RequestParam("index") String index){
        esUtil.createIndex(index);
    }

}
