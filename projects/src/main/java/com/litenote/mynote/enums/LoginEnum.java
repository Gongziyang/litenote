package com.litenote.mynote.enums;

import lombok.Getter;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/17 20:38
 * @UpdateDate: 2022/2/17 20:38
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Getter
public enum LoginEnum {

    /**
     * 登录成功
     */
    LOGIN_SUCCESS(0, "登录成功"),

    /**
     * 用户信息不存在
     */
    USER_INFO_NOT_EXSIT(1, "登录失败，用户信息不存在");

    private LoginEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }


    private final Integer code;

    private final String desc;
}
