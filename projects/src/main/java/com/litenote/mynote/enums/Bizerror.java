package com.litenote.mynote.enums;

import lombok.Getter;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/12 20:16
 * @UpdateDate: 2022/2/12 20:16
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 业务异常信息
 */
@Getter
public enum Bizerror {

    /**
     * 笔记名称已存在
     */
    NOTE_NAME_EXSITZ("L0001", "笔记名称已存在！"),

    /**
     * get请求调用失败
     */
    REQ_BAD("L0002", "http请求调用失败！"),

    /**
     * 请求参数错误
     */
    PARAMS_ERROR("L0003", "请求参数错误"),

    /**
     * 拉取用户信息失败
     */
    USER_INFO_ERROR("L0004", "获取session_key失败！"),

    /**
     * 文件上传失败
     */
    FILE_UPLOAD_FAIL("L0005", "文件上传失败！"),

    /**
     * 用户未登录
     */
    AUTHORIZATION_FAILED("L0007", "用户未登录，找不到token信息"),

    /**
     * 登录凭证已过期
     */
    TOKEN_EXPIRED("L0008", "登录已过期，请重新登录"),

    /**
     * 消息发送失败
     */
    MESSAGE_SEND_FAILURE("L0009", "消息发送失败"),

    /**
     * 用户没有权限
     */
    NO_PERMISSION("L00010", "暂无权限"),

    /**
     * 索引名称不能为空！
     */
    ES_NAME_ISEMPTY("L00011", "索引名称不能为空");

    /**
     * 状态码
     */
    private final String code;

    /**
     * 错误描述
     */
    private final String desc;

    Bizerror(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
