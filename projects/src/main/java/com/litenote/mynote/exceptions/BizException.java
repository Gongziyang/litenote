package com.litenote.mynote.exceptions;

import com.litenote.mynote.enums.Bizerror;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/12 20:25
 * @UpdateDate: 2022/2/12 20:25
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 业务异常类
 */
public class BizException extends RuntimeException {

    private static final long serialVersionUID = 4941120614090564281L;

    private String code;
    private String message;

    public BizException(Bizerror bizerror) {
        super(bizerror.getDesc());
        this.code = bizerror.getCode();
        this.message = bizerror.getCode();
    }

    public BizException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }


    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "BizException{code='" + this.code + '\'' + ", message='" + this.message + '\'' + '}';
    }
}
