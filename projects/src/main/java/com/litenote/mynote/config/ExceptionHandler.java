package com.litenote.mynote.config;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/20 20:35
 * @UpdateDate: 2022/2/20 20:35
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@ControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
    @ResponseBody
    public Map<String, Object> exeHandler(ServletRequest servletRequest, Exception e) {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        Map<String, Object> result = new HashMap<>(4);
        result.put("timestamp", new Date());
        result.put("status", 500);
        result.put("path", request.getServletPath());
        result.put("error", e.getMessage());
        return result;
    }
}
