package com.litenote.mynote.constant;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/18 12:23
 * @UpdateDate: 2022/2/18 12:23
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class CommonConstant {

    public static final String USER_LOGIN_KEY = "WX:USER_AUTH_SESSION_CODE:";

    public static final String ACCESS_TOKEN = "Access-Token";

    /**
     * 登录过期时间30分钟
     */
    public static final Integer EXPIRE_TIME = 1800;
}
