package com.litenote.mynote.filter;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.exceptions.BizException;
import com.litenote.mynote.util.BeanUtil;
import com.litenote.mynote.util.RedisUtils;
import com.litenote.mynote.constant.*;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/18 12:12
 * @UpdateDate: 2022/2/18 12:12
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu //
 */
public class LoginFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        RedisUtils redisUtils = BeanUtil.getBean(RedisUtils.class, "redisUtils");
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String accessToken = request.getHeader(CommonConstant.ACCESS_TOKEN);
        if (StrUtil.isEmpty(accessToken)) {
            throw new BizException(Bizerror.AUTHORIZATION_FAILED);
        }
        Object token = redisUtils.get(CommonConstant.USER_LOGIN_KEY + accessToken.trim());
        if (ObjectUtil.isNull(token)) {
            throw new BizException(Bizerror.TOKEN_EXPIRED);
        }
        //刷新token有效期
        redisUtils.expire(accessToken.trim(), CommonConstant.EXPIRE_TIME);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }
}
