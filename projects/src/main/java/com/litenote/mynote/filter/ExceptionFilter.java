package com.litenote.mynote.filter;

import com.litenote.mynote.exceptions.BizException;

import javax.servlet.*;
import java.io.IOException;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/21 10:55
 * @UpdateDate: 2022/2/21 10:55
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 过滤器异常处理过滤类
 */
public class ExceptionFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        try {
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (BizException e) {
            servletRequest.setAttribute("filter.error", e);
            //跳转到控制器，由控制器抛出异常。
            servletRequest.getRequestDispatcher("/exception_handle/catch").forward(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
