package com.litenote.mynote.autoconfig;

import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/28 17:05
 * @UpdateDate: 2022/2/28 17:05
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
public class EnvironmentConfig implements EnvironmentAware {

    private Environment newE;

    @Override
    public void setEnvironment(Environment environment) {
        this.newE = environment;
    }
    
}
