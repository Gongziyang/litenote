package com.litenote.mynote.autoconfig;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.PropertyResolver;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/28 17:06
 * @UpdateDate: 2022/2/28 17:06
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Configuration
@ConditionalOnClass(PropertyResolver.class)
public class EnvironmentAutoConfig {

    @Bean
    EnvironmentConfig environmentConfig(){
        return new EnvironmentConfig();
    }
}
