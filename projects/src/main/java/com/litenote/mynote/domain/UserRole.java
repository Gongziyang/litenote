package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

import lombok.Data;

/**
 * @TableName user_role
 */
@TableName(value = "user_role")
@Data
public class UserRole implements Serializable {
    private static final long serialVersionUID = 3513291051989052526L;
    /**
     * 主键
     */
    @TableId
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 是否为超管 0是 1否
     */
    private Integer role;

}