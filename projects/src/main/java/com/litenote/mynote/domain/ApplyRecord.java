package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 
 * @TableName apply_record
 */
@TableName(value ="apply_record")
@Data
public class ApplyRecord implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 笔记ID
     */
    private Long noteId;

    /**
     * 创建时间
     */
    private Date updateTime;

    /**
     * 更新时间
     */
    private Date createTime;

    /**
     * 是否删除
     */
    private Integer hasDelete;

}