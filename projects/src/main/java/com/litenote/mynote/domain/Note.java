package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName note
 */
@TableName(value = "note")
@Data
public class Note implements Serializable {
    private static final long serialVersionUID = -7125262400274721892L;
    /**
     * 笔记id（主键）
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 笔记名称
     */
    private String name;

    /**
     * 是否删除 0否 1是
     */
    private Byte hasDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 笔记内简要容
     */
    private String contextBrief;

    /**
     * 笔记记录时间
     */
    private Date noteTime;
}