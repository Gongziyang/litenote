package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName note_detail
 */
@TableName(value = "note_detail")
@Data
public class NoteDetail implements Serializable {
    private static final long serialVersionUID = 3952843053784862788L;
    /**
     * 笔记扩展信息id
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * note_id关联笔记id
     */
    private Long noteId;

    /**
     * 是否被删除 0否1 是
     */
    private Byte hasDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 笔记记录时间
     */
    private Date noteTime;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 笔记内容
     */
    private String context;
}