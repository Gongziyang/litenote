package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName user__info
 */
@TableName(value = "user__info")
@Data
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 3517349229958636243L;
    /**
     * 个人信息id（主建）
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 微信授权union_id
     */
    private String unionId;

    /**
     * 头像地址
     */
    private String avatar;

    /**
     * 是否被删除 0否 1 是
     */
    private Boolean hasDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 微信用户openID
     */
    private String openId;

    /**
     * 用户性别
     */
    private String gender;

    /**
     * 用户手机号
     */
    private String phoneNumber;

    /**
     * 国家
     */
    private String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

}