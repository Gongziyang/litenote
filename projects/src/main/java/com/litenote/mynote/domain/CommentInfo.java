package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName comment_info
 */
@TableName(value = "comment_info")
@Data
public class CommentInfo implements Serializable {
    private static final long serialVersionUID = 1953806755313581783L;
    /**
     * 笔记评论
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 笔记id
     */
    private Long noteId;

    /**
     * 评论内容
     */
    private String context;

    /**
     * 是否删除 0否 1是
     */
    private Byte hasDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 用户Id
     */
    private Long userId;

}