package com.litenote.mynote.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * @TableName picture
 */
@TableName(value = "picture")
@Data
public class Picture implements Serializable {

    private static final long serialVersionUID = 2050265854021618373L;
    /**
     * 图片ID
     */
    @TableId(type = IdType.AUTO)
    private Long pictureId;

    /**
     * 笔记ID
     */
    private Long noteId;

    /**
     * 图片说明
     */
    private String info;

    /**
     * 图片地址
     */
    private String pictureUrl;

    /**
     * 是否删除 0否 1是
     */
    private Integer hasDelete;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;
}