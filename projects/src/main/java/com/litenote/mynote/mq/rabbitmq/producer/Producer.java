package com.litenote.mynote.mq.rabbitmq.producer;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.UUID;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/3 17:03
 * @UpdateDate: 2022/3/3 17:03
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Component
@Slf4j
public class Producer implements RabbitTemplate.ConfirmCallback, RabbitTemplate.ReturnsCallback {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    /**
     * 普通发送模式
     *
     * @param message
     */
    public void sendMessage(String message) {
        rabbitTemplate.convertAndSend("router-demo", "binding-keyA", message);
        log.info("消息发送成功：{}", message);
    }

    /**
     * 带消息补偿机制发送模式
     */
    public void sendMessageWithCompensate(String exchange, String routingKey, String content) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setCorrelationId(UUID.randomUUID().toString());
        Message message = new Message(content.getBytes(), messageProperties);
        CorrelationData correlationData = new CorrelationData();
        correlationData.setId(messageProperties.getCorrelationId());
        rabbitTemplate.setReturnsCallback(this);
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.convertAndSend(exchange, routingKey, message, correlationData);
    }

    /**
     * 生产者发送消息后会回调该方法，ack为true代表成功
     *
     * @param correlationData
     * @param ack
     * @param cause
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        if (ack) {
            log.info("消息发送成功，发送的消息为：{}", JSONObject.toJSONString(correlationData));
        } else {
            //根据correlationData里面的数据来进行重试补偿
            ReturnedMessage returned = correlationData.getReturned();
            String exchange = returned.getExchange();
            String routingKey = returned.getRoutingKey();
            if (StrUtil.isEmpty((String) redisTemplate.opsForValue().get(correlationData.getId()))) {
                redisTemplate.opsForValue().set(correlationData.getId(), "1");
            }
            redisTemplate.opsForValue().increment(correlationData.getId(), 1);
            if (Integer.parseInt((String) redisTemplate.opsForValue().get(correlationData.getId())) >= 3) {
                //达到重试上限，丢弃消息
                log.info("消息达到重试上限，不作处理");
                return;
            }
            rabbitTemplate.convertAndSend(exchange, routingKey, returned.getMessage(), correlationData);
            log.info("消息已重新发送");
        }
    }

    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        Message message = returnedMessage.getMessage();
        String routingKey = returnedMessage.getRoutingKey();
        String exchange = returnedMessage.getExchange();
        String correlationId = message.getMessageProperties().getCorrelationId();
        String countStr = (String) redisTemplate.opsForValue().get(correlationId);
        if (ObjectUtil.isNull(countStr)) {
            redisTemplate.opsForValue().set(correlationId, "0");
        }
        countStr = (String) redisTemplate.opsForValue().get(correlationId);
        if (Integer.parseInt(countStr) >= 3) {
            //达到重试上限，丢弃消息
            log.info("消息达到重试上限，不作处理");
            return;
        }
        CorrelationData correlationData = new CorrelationData();
        ReturnedMessage returnedMessage1 = new ReturnedMessage(message, -1, "", "router-demo", "binding-keyA");
        correlationData.setReturned(returnedMessage1);
        correlationData.setId(correlationId);
        rabbitTemplate.convertAndSend(exchange, routingKey, message, correlationData);
        redisTemplate.opsForValue().increment(correlationId, 1);
        log.info("消息已重新发送");
    }

    @Override
    public void returnedMessage(Message message, int i, String s, String s1, String s2) {

    }
}
