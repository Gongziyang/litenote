package com.litenote.mynote.mq.kafka;

import com.litenote.mynote.mq.kafka.producer.NoteProducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 18:54
 * @UpdateDate: 2022/3/2 18:54
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@RestController
@RequestMapping("/note/message/test/")
public class MessageController {

    @Autowired
    NoteProducer noteProducer;

    @RequestMapping("send")
    public void send(@RequestParam("topic") String topic
            , @RequestParam("message") String message
            , @RequestParam("method") Integer method) {
        switch (method) {
            case 0:
                noteProducer.send(topic, message);
                break;
            case 1:
                noteProducer.sendCallBack(topic, message);
                break;
            case 2:
                noteProducer.sendInTransaction(topic, message);
                break;
            default:
                return;
        }
    }
}
