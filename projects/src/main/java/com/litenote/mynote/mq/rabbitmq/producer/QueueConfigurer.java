package com.litenote.mynote.mq.rabbitmq.producer;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/3 17:01
 * @UpdateDate: 2022/3/3 17:01
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Configuration
public class QueueConfigurer {

    /**
     * 创建队列
     * durable : 持久化 默认为true
     *
     * @return
     */
    @Bean
    public Queue queueA() {
        return new Queue("lite-note-A", true);
    }

    @Bean
    public Queue queueB() {
        return new Queue("lite-note-B", true);
    }


    /**
     * ********************************* 发布订阅模式：fanout交换机 *********************************
     * durable : 持久化 默认为true
     *
     * @return
     */
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("publisher-subscript-demo", true, true);
    }

    @Bean
    public Binding bindingA(Queue queueA, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueA).to(fanoutExchange);
    }

    @Bean
    public Binding bindingB(Queue queueB, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(queueB).to(fanoutExchange);
    }


    /**
     * ********************************* 路由模式 ： direct交换机 *********************************
     *
     * @return
     */
    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("router-demo");
    }

    @Bean
    public Binding dBindingA(Queue queueA, DirectExchange directExchange) {
        return BindingBuilder.bind(queueA).to(directExchange).with("binding-keyA");
    }

    @Bean
    public Binding dBindingB(Queue queueB, DirectExchange directExchange) {
        return BindingBuilder.bind(queueB).to(directExchange).with("binding-keyB");
    }


    /**
     * ****************************** 主题模式 ： topic交换机  *********************************
     */
    @Bean
    public TopicExchange topicExchange() {
        return new TopicExchange("topic-exchanger-demo");
    }

    @Bean
    public Binding tBindingA(Queue queueA, TopicExchange topicExchange) {
        return BindingBuilder.bind(queueA).to(topicExchange).with("*.topicA.*");
    }

    @Bean
    public Binding tBindingB(Queue queueB, TopicExchange topicExchange) {
        return BindingBuilder.bind(queueB).to(topicExchange).with("*.topicB.*");
    }
}
