package com.litenote.mynote.mq.kafka.producer;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSONObject;
import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.exceptions.BizException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 17:23
 * @UpdateDate: 2022/3/2 17:23
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Component
@Slf4j
public class NoteProducer {

    @Value("${spring.kafka.properties.topics.test-1}")
    private String topic;

    @Autowired
    KafkaTemplate<String, Object> kafkaTemplate;

    /**
     * 普通发送
     */
    public void send(String topic, Object message) {
        kafkaTemplate.send(ObjectUtil.isEmpty(topic) ? this.topic : topic, message);
        log.info("发送消息：{}", JSONObject.toJSONString(message));
    }

    /**
     * 带回调发送
     */
    public void sendCallBack(String topic, Object message) {
        kafkaTemplate.send(ObjectUtil.isEmpty(topic) ? this.topic : topic, message).addCallback(new ListenableFutureCallback<SendResult<String, Object>>() {
            @Override
            public void onFailure(Throwable throwable) {
                log.info("消息发送失败；{}", JSONObject.toJSONString(message));
                throw new BizException(Bizerror.MESSAGE_SEND_FAILURE);
            }

            @Override
            public void onSuccess(SendResult<String, Object> stringObjectSendResult) {
                log.info("消息发送成功：{}", JSONObject.toJSONString(message));
                log.info("发送topic:{},分区:{},offset:{}"
                        , stringObjectSendResult.getRecordMetadata().topic()
                        , stringObjectSendResult.getRecordMetadata().partition()
                        , stringObjectSendResult.getRecordMetadata().offset());
            }
        });
    }

    /**
     * 事务发送
     */
    public void sendInTransaction(String topic, Object message) {
        kafkaTemplate.executeInTransaction(action -> {
            action.send(ObjectUtil.isEmpty(topic) ? this.topic : topic, message);
            log.info("事务发送：{}", JSONObject.toJSONString(message));
            throw new BizException(Bizerror.MESSAGE_SEND_FAILURE);
        });
    }

}
