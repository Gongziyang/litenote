package com.litenote.mynote.mq.kafka.consumer;

import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.exceptions.BizException;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.PartitionOffset;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 17:22
 * @UpdateDate: 2022/3/2 17:22
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Component
@Slf4j
public class NoteConsumer {

    @Value("${spring.kafka.properties.topics.test-1}")
    private String topic;


    /**
     * 普通监听
     */
    @KafkaListener(id = "1", topics = {"note-lite-test-topic1"})
    public void consume(ConsumerRecord<?, ?> record) {
        String s = String.valueOf(record.value());
        log.info("收到消息：{},topic:{},分区：{}，offset:{}", s, record.topic(), record.partition(), record.offset());
    }

    /**
     * 指定分区或者off-set消费
     */
    @KafkaListener(id = "2", topicPartitions = {
            @TopicPartition(topic = "note-lite-test-topic2", partitions = {"0"}, partitionOffsets = {
                    @PartitionOffset(partition = "1", initialOffset = "2")
            }),
            @TopicPartition(topic = "note-lite-test-topic2", partitions = {"3,4"}, partitionOffsets = {
                    @PartitionOffset(partition = "6", initialOffset = "2"),
                    @PartitionOffset(partition = "5", initialOffset = "2")
            })
    })
    public void consumeCustom(ConsumerRecord<?, ?> record) {
        String s = String.valueOf(record.value());
        log.info("收到消息：{},topic:{},分区：{}，offset:{}", s, record.topic(), record.partition(), record.offset());
    }

    /**
     * 批量读取
     */
    @KafkaListener(id = "3", topics = {"note-lite-test-topic3"})
    public void consumerBatch(List<ConsumerRecord<?, ?>> records) {

    }

    /**
     * 消息过滤
     */
    @KafkaListener(id = "4", topics = {"note-lite-test-topic4"}, containerFactory = "containerFactory")
    public void consumerWithFilter(ConsumerRecord<?, ?> record) {
        String s = String.valueOf(record.value());
        log.info("收到消息：{},topic:{},分区：{}，offset:{}", s, record.topic(), record.partition(), record.offset());
    }


    /**
     * 异常处理
     */
    @KafkaListener(id = "5", topics = {"note-lite-test-topic5"}, errorHandler = "errorHandler")
    public void consumerWithError(ConsumerRecord<?, ?> record) {
        String s = String.valueOf(record.value());
        log.info("收到消息：{},topic:{},分区：{}，offset:{}", s, record.topic(), record.partition(), record.offset());
        throw new BizException(Bizerror.MESSAGE_SEND_FAILURE);
    }

    /**
     * 消息转发
     */
    @KafkaListener(id = "6", topics = {"note-lite-test-topic6"})
    @SendTo("note-lite-test-topic2")
    public void consumerWithRedirect(ConsumerRecord<?, ?> record) {
        String s = String.valueOf(record.value());
        log.info("收到消息：{},topic:{},分区：{}，offset:{}", s, record.topic(), record.partition(), record.offset());
    }
}
