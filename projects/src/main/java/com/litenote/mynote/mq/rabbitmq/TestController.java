package com.litenote.mynote.mq.rabbitmq;

import com.litenote.mynote.mq.rabbitmq.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/4 16:07
 * @UpdateDate: 2022/3/4 16:07
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@RestController
@RequestMapping("/note/rabbit/")
public class TestController {

    @Autowired
    Producer producer;

    @RequestMapping("testSend")
    public void send(@RequestParam("routingKey") String routingKey
            , @RequestParam("exchange") String exchange
            , @RequestParam("content") String content) {
        producer.sendMessageWithCompensate(exchange, routingKey, content);
    }
}
