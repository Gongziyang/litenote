package com.litenote.mynote.mq.kafka.consumer.config;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.listener.ConsumerAwareListenerErrorHandler;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 17:22
 * @UpdateDate: 2022/3/2 17:22
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Component
@Slf4j
public class ConsumerConfig {

    @Autowired
    ConsumerFactory consumerFactory;

    /**
     * 消息过滤
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory containerFactory() {
        ConcurrentKafkaListenerContainerFactory listenerContainerFactory = new ConcurrentKafkaListenerContainerFactory();
        listenerContainerFactory.setConsumerFactory(consumerFactory);
        listenerContainerFactory.setAckDiscarded(true);
        listenerContainerFactory.setRecordFilterStrategy(item -> {
            if (Integer.valueOf(item.value().toString()) > 5) {
                return false;
            }
            //返回true表示被拦截
            return true;
        });
        return listenerContainerFactory;
    }

    /**
     * 异常处理
     */
    @Bean
    public ConsumerAwareListenerErrorHandler errorHandler() {
        return (message, exe, consumer) -> {
            log.info("发送消息：{}，异常信息：{}，消费者信息：{}", JSONObject.toJSONString(message)
                    , exe.getStackTrace()
                    , JSONObject.toJSONString(consumer));
            return null;
        };
    }
}
