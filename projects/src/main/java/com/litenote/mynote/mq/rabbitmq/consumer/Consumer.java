package com.litenote.mynote.mq.rabbitmq.consumer;

import cn.hutool.core.util.ObjectUtil;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/3 17:07
 * @UpdateDate: 2022/3/3 17:07
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Component
@Slf4j
public class Consumer {

//    @RabbitHandler
//    public void consume(String message) {
//        log.info("消息消费成功：{}", message);
//    }

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    @RabbitHandler
    @RabbitListener(queues = {"lite-note-A"})
    public void consume(Message message, Channel channel) throws IOException {
        String messageStr = new String(message.getBody());
        log.info("收到消息：{}", messageStr);
        log.info("开始消费...");
        if (ObjectUtil.isNull(redisTemplate.opsForValue().get(message.getMessageProperties().getCorrelationId()))) {
            redisTemplate.opsForValue().set(message.getMessageProperties().getCorrelationId(), "0");
        }
        redisTemplate.opsForValue().increment(message.getMessageProperties().getCorrelationId(), 1);
        String countStr = (String) redisTemplate.opsForValue().get(message.getMessageProperties().getCorrelationId());
        if (Integer.parseInt(countStr) >= 3) {
            log.info("达到最大重试次数，抛弃消息");
            channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
            return;
        }

        log.info("收到消息：{}", message.getBody().toString());
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
