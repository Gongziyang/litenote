package com.litenote.mynote.mq.kafka.producer;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 18:27
 * @UpdateDate: 2022/3/2 18:27
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Configuration
public class CreateTopic {

    @Value("${spring.kafka.properties.topics.test-1}")
    private String topic1;
    @Value("${spring.kafka.properties.topics.test-2}")
    private String topic2;
    @Value("${spring.kafka.properties.topics.test-3}")
    private String topic3;
    @Value("${spring.kafka.properties.topics.test-4}")
    private String topic4;
    @Value("${spring.kafka.properties.topics.test-5}")
    private String topic5;
    @Value("${spring.kafka.properties.topics.test-6}")
    private String topic6;

    @Bean
    public NewTopic topic1(){
        return new NewTopic(topic1,1,(short) 1);
    }

    @Bean
    public NewTopic topic2(){
        return new NewTopic(topic2,1,(short) 1);
    }

    @Bean
    public NewTopic topic3(){
        return new NewTopic(topic3,1,(short) 1);
    }

    @Bean
    public NewTopic topic4(){
        return new NewTopic(topic4,1,(short) 1);
    }

    @Bean
    public NewTopic topic5(){
        return new NewTopic(topic5,1,(short) 1);
    }

    @Bean
    public NewTopic topic6(){
        return new NewTopic(topic6,1,(short) 1);
    }

}
