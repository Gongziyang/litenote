package com.litenote.mynote.mq.rabbitmq.consumer;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/3 20:21
 * @UpdateDate: 2022/3/3 20:21
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Component
@Slf4j
public class Consumer1 {

    @RabbitHandler
    @RabbitListener(queues = {"lite-note-B"})
    public void consumeA(Message message, Channel channel) throws IOException {
        byte[] body = message.getBody();
        String messageStr = new String(body);
        log.info("lite-note-B - consumeA 收到一条消息：{}", messageStr);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {

        }
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }

    @RabbitHandler
    @RabbitListener(queues = {"lite-note-B"})
    public void consumeB(Message message, Channel channel) throws IOException {
        byte[] body = message.getBody();
        String messageStr = new String(body);
        log.info("lite-note-B - consumeB 收到一条消息：{}", messageStr);
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
    }
}
