package com.litenote.mynote.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litenote.mynote.domain.ApplyRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litenote.mynote.vo.ApplyVo;
import com.litenote.mynote.vo.BaseVo;
import com.litenote.mynote.vo.CommentVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Entity com.litenote.mynote.domain.ApplyRecord
 */
@Mapper
public interface ApplyRecordMapper extends BaseMapper<ApplyRecord> {

    List<ApplyVo> queryApplyUsers(@Param("page") Page<ApplyVo> page, @Param("noteId") Long noteId);
}




