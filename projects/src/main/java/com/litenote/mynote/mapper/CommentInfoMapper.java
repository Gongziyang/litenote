package com.litenote.mynote.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.litenote.mynote.domain.*;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.litenote.mynote.vo.*;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Entity com.litenote.mynote.domain.CommentInfo
 */
@Mapper
public interface CommentInfoMapper extends BaseMapper<CommentInfo> {

    List<CommentVo> getCommentById(@Param("page") Page<CommentVo> page, @Param("noteId") Long noteId);

}




