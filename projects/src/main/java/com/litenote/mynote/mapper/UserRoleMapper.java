package com.litenote.mynote.mapper;

import com.litenote.mynote.domain.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.litenote.mynote.domain.UserRole
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {

}




