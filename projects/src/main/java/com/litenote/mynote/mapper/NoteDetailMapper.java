package com.litenote.mynote.mapper;

import com.litenote.mynote.domain.NoteDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.litenote.mynote.domain.NoteDetail
 */
@Mapper
public interface NoteDetailMapper extends BaseMapper<NoteDetail> {

}




