package com.litenote.mynote.mapper;

import com.litenote.mynote.domain.Note;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.litenote.mynote.domain.Note
 */
@Mapper
public interface NoteMapper extends BaseMapper<Note> {

    Long getTotal();
}




