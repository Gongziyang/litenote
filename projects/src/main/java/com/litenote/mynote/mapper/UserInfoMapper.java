package com.litenote.mynote.mapper;

import com.litenote.mynote.domain.UserInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Entity com.litenote.mynote.domain.UserInfo
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    Integer isMaster(@Param("unionId") String unionId, @Param("openId") String openId);

    Integer getUserRole(@Param("userId") Long userId);
}




