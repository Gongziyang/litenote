package com.litenote.mynote.mapper;

import com.litenote.mynote.domain.Picture;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.litenote.mynote.domain.Picture
 */
@Mapper
public interface PictureMapper extends BaseMapper<Picture> {

}




