package com.litenote.mynote.qo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:43
 * @UpdateDate: 2022/2/11 23:43
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 保存笔记实体
 */
@Data
@ApiModel("保存笔记实体类")
public class SaveNoteQo {

    /**
     * 笔记Id
     */
    @ApiModelProperty(name = "id", value = "笔记唯一标识", dataType = "Long", example = "2356895425125462")
    private Long id;

    /**
     * 笔记名称
     */
    @ApiModelProperty(name = "noteName", value = "笔记名称", dataType = "String", example = "情人节")
    @NotNull(message = "请输入笔记名称！")
    private String noteName;

    /**
     * 笔记简要信息
     */
    @ApiModelProperty(name = "context", value = "笔记信息", dataType = "String", example = "恋爱日记")
    @JsonProperty(value = "context")
    private String contextBrief;

    /**
     * 记录笔记时间
     */
    @ApiModelProperty(name = "noteTime", value = "记录笔记时间", dataType = "String", example = "2022-01-05")
    private Date noteTime;
}
