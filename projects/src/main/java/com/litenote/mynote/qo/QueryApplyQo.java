package com.litenote.mynote.qo;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 20:24
 * @UpdateDate: 2022/3/2 20:24
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class QueryApplyQo {

    private Long noteId;

    private Integer pageNo;

    private Integer pageSize;
}
