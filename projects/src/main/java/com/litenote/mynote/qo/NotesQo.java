package com.litenote.mynote.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:16
 * @UpdateDate: 2022/2/11 23:16
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 笔记列表请求实体
 */
@Data
@ApiModel("笔记列表查询实体类")
public class NotesQo {

    /**
     * 笔记名称
     */
    @ApiModelProperty(name = "noteName", value = "笔记名称", dataType = "String", example = "02154541512")
    private String noteName;

    /**
     * 页面大小
     */
    @ApiModelProperty(name = "pageSize", value = "页面大小", dataType = "Integer", example = "5")
    private Integer pageSize;

    /**
     * 当前页码
     */
    @ApiModelProperty(name = "pageNo", value = "当前页码", dataType = "Integer", example = "1")
    private Integer pageNo;
}
