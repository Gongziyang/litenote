package com.litenote.mynote.qo.wx.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/16 23:00
 * @UpdateDate: 2022/2/16 23:00
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu login result
 */
@Data
public class AuthVo implements Serializable {

    private static final long serialVersionUID = -2795952578769468331L;
    /**
     * 用户唯一标识
     */
    private String openid;

    /**
     * 会话密钥
     */
    private String session_key;

    /**
     * 用户在开放平台的唯一标识符，若当前小程序已绑定到微信开放平台帐号下会返回，详见 UnionID 机制说明。
     */
    private String unionid;

    /**
     * 错误码 0请求成功 -1系统繁忙，此时请开发者稍候再试 40029code无效 45011频率限制，每个用户每分钟100次 40226高风险等级用户，小程序登录拦截
     */
    private String errcode;

    /**
     * 错误信息
     */
    private String errmsg;

    /**
     * 用户业务ID
     */
    private Long userId;

    /**
     * 是否为超管 0是 1否
     */
    private Integer isMaster;

    /**
     * 状态
     */
    private Integer code;
}
