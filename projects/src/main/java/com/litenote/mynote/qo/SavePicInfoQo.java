package com.litenote.mynote.qo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/18 20:34
 * @UpdateDate: 2022/2/18 20:34
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class SavePicInfoQo {

    /**
     * 图片ID
     */
    @NotNull(message = "图片ID不能为null")
    private Long pictureId;

    /**
     * 图片说明
     */
    private String info;
}
