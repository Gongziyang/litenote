package com.litenote.mynote.qo.wx.qo;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/16 22:39
 * @UpdateDate: 2022/2/16 22:39
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 身份验证qo
 */
@Data
public class AuthQo {
    /**
     * 小程序登录调用 wx.login接口返回的 code
     */
    private String js_code;

    /**
     * 微信用户昵称
     */
    private String nickName;

    /**
     * 头像地址
     */
    private String avatarUrl;

}
