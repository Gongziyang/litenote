package com.litenote.mynote.qo;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/1 21:59
 * @UpdateDate: 2022/3/1 21:59
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class ApplyQo {

    /**
     * 笔记ID
     */
    @NotNull(message = "笔记ID不能为空")
    private Long noteId;

    /**
     * 用户ID
     */
    @NotNull(message = "用户ID不能为空")
    private Long userId;
}
