package com.litenote.mynote.qo;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/17 20:12
 * @UpdateDate: 2022/2/17 20:12
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class LoginQo {

    /**
     * 微信用户昵称
     */
    private String nickName;

    /**
     * 头像地址
     */
    private String avatarUrl;

    /**
     * 性别 0未知 1男 2女
     */
    private Integer gender;

    /**
     * 省
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 国家
     */
    private String country;
}
