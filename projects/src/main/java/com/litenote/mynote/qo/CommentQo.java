package com.litenote.mynote.qo;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/16 14:40
 * @UpdateDate: 2022/2/16 14:40
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 评论实体
 */
@Data
public class CommentQo {

    private Long noteId;

    private String context;

    private Long timestamp;

    private Long userId;
}
