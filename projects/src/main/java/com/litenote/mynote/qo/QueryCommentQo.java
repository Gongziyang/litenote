package com.litenote.mynote.qo;

import lombok.Data;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/1 21:43
 * @UpdateDate: 2022/3/1 21:43
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 查询评论请求体
 */
@Data
public class QueryCommentQo {

    private Long noteId;

    private Integer pageNo;

    private Integer pageSize;
}
