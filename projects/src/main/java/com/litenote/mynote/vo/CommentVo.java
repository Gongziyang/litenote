package com.litenote.mynote.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:50
 * @UpdateDate: 2022/2/11 23:50
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 评论实体
 */
@Data
public class CommentVo {

    /**
     * 评论ID
     */
    private String id;

    /**
     * 评论内容
     */
    private String context;

    /**
     * 用户唯一标识
     */
    private String unionId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 评论时间
     */
    private Date createTime;

    /**
     * 评论时间
     */
    private String userId;

}
