package com.litenote.mynote.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:24
 * @UpdateDate: 2022/2/11 23:24
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class NoteVo {

    private String id;

    private String name;

    private Date createTime;

    private Date updateTime;

    private Date noteTime;

    private String contextBrief;

    private Long timestamp;

    private List<String> picUrls;
}
