package com.litenote.mynote.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/12 19:35
 * @UpdateDate: 2022/2/12 19:35
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class BaseVo<T> {

    /**
     * 总页面数
     */
    private Long pages;

    /**
     * 当前页码
     */
    private Integer pageNo;

    /**
     * 页面大小
     */
    private Integer pageSize;

    /**
     * 数据对象
     */
    private T data;

    /**
     * 总数据条目数
     */
    private Long total;

    /**
     * 操作状态 0成功 1失败
     */
    private Integer code;

    /**
     * 扩展字段 新建笔记时值为笔记ID
     */
    private Object extendVal;

    /**
     * 评论时间
     */
    private Date createTime;

}
