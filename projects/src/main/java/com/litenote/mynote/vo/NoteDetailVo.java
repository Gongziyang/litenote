package com.litenote.mynote.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/2/11 23:54
 * @UpdateDate: 2022/2/11 23:54
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu 笔记详情实体
 */
@Data
public class NoteDetailVo {

    /**
     * 笔记ID
     */
    private String noteId;

    /**
     * 评论数量
     */
    private Integer commentNumber;

    /**
     * 点赞数量
     */
    private Integer clickNumber;

    /**
     * 时间戳
     */
    private Long timestamp;

    /**
     * 笔记内容
     */
    private String context;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 更新时间
     */
    private Date createTime;

    /**
     * 笔记记录时间
     */
    private Date noteTime;

    /**
     * 图片
     */
    private List<String> picUrls;

    /**
     * 当前用户是否点赞 0否 1是
     */
    private Integer isClick;
}
