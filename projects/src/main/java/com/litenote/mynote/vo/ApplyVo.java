package com.litenote.mynote.vo;

import lombok.Data;

import java.util.Date;

/**
 * @Description:
 * @Copyright: Copyright (c) 2019  ALL RIGHTS RESERVED.
 * @Company: feng
 * @Author: 杨一峰
 * @CreateDate: 2022/3/2 20:25
 * @UpdateDate: 2022/3/2 20:25
 * @UpdateRemark: init
 * @Version: 1.0
 * @menu
 */
@Data
public class ApplyVo {
    /**
     * 点赞ID
     */
    private String id;

    /**
     * 用户唯一标识
     */
    private String unionId;

    /**
     * 用户昵称
     */
    private String nickName;

    /**
     * 用户头像
     */
    private String avatar;

    /**
     * 点赞时间
     */
    private Date createTime;

    /**
     * 用户ID
     */
    private String userId;

}
