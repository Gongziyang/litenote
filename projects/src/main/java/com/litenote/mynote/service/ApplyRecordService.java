package com.litenote.mynote.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.domain.ApplyRecord;
import com.litenote.mynote.mapper.ApplyRecordMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ApplyRecordService extends ServiceImpl<ApplyRecordMapper, ApplyRecord> {

}




