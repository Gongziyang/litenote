package com.litenote.mynote.service;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.domain.Picture;
import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.exceptions.BizException;
import com.litenote.mynote.mapper.PictureMapper;
import com.litenote.mynote.qo.SavePicInfoQo;
import com.litenote.mynote.util.IdWorker;
import com.litenote.mynote.vo.BaseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.*;

/**
 *
 */
@Service
public class PictureService extends ServiceImpl<PictureMapper, Picture> {

    @Autowired
    IdWorker idWorker;

    @Value("${file.rootPath}")
    private String ROOT_PATH;

    @Value("${file.picPath}")
    private String PIC_PATH;

    @Value("${file.host}")
    private String HOST;

    /**
     * 保存图片
     *
     * @param file
     * @param noteId
     * @return
     */
    public BaseVo savePicture(MultipartFile file, Long noteId) {
        //保存文件
        String fileName = saveFile(file);
        //落库
        BaseVo baseVo = new BaseVo();
        baseVo.setCode(0);
        Picture picture = new Picture();
        picture.setPictureId(idWorker.nextId(null).longValue());
        picture.setNoteId(noteId);
        picture.setPictureUrl(HOST + PIC_PATH + fileName);
        picture.setCreateTime(new Date());
        picture.setUpdateTime(picture.getCreateTime());
        boolean save = this.save(picture);
        if (Boolean.FALSE.equals(save)) {
            baseVo.setCode(-1);
        }
        baseVo.setExtendVal(picture.getPictureId());
        return baseVo;
    }

    public String saveFile(MultipartFile file) {
        String filePath = ROOT_PATH + PIC_PATH;
        String prefix = file.getOriginalFilename();
        String suffix = prefix.substring(prefix.lastIndexOf(".") + 1);
        Random random = new Random();
        Integer randomName = random.nextInt(1000);
        String filePathName = idWorker.nextId(null).longValue() + randomName + suffix + "." + suffix;

        File targetFile = new File(filePath + filePathName);
        if (!targetFile.getParentFile().exists()) {
            targetFile.getParentFile().mkdirs();
        }
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            throw new BizException(Bizerror.FILE_UPLOAD_FAIL);
        }
        return filePathName;
    }

    /**
     * 保存图片说明信息
     *
     * @param savePicInfoQo
     * @return
     */
    public BaseVo savePictureInfo(SavePicInfoQo savePicInfoQo) {
        Optional<Picture> picture = this.lambdaQuery().eq(Picture::getPictureId, savePicInfoQo.getPictureId()).last("limit 1").oneOpt();
        BaseVo baseVo = new BaseVo();
        baseVo.setCode(0);
        if (picture.isPresent()) {
            picture.get().setInfo(savePicInfoQo.getInfo());
            boolean b = this.updateById(picture.get());
            if (Boolean.FALSE.equals(b)) {
                baseVo.setCode(-1);
            }
        }
        return baseVo;
    }

    /**
     * 删除图片
     *
     * @param pictureId
     * @return
     */
    public BaseVo deletePicture(@RequestParam("pictureId") Long pictureId) {
        this.getBaseMapper().deleteById(pictureId);
        BaseVo baseVo = new BaseVo();
        baseVo.setCode(0);
        return baseVo;
    }

    public BaseVo getPictureUrls(Long noteId) {
        List<Picture> list = this.lambdaQuery().eq(Picture::getNoteId, noteId).list();
        BaseVo<List<Picture>> baseVo = new BaseVo<>();
        if (CollUtil.isNotEmpty(list)) {
            baseVo.setCode(0);
            baseVo.setData(list);
            return baseVo;
        }
        baseVo.setData(Collections.emptyList());
        return baseVo;
    }

}



