package com.litenote.mynote.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.constant.CommonConstant;
import com.litenote.mynote.domain.UserInfo;
import com.litenote.mynote.domain.UserRole;
import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.exceptions.BizException;
import com.litenote.mynote.mapper.UserInfoMapper;
import com.litenote.mynote.qo.wx.qo.AuthQo;
import com.litenote.mynote.qo.wx.vo.AuthVo;
import com.litenote.mynote.util.HttpUtils;
import com.litenote.mynote.util.IdWorker;
import com.litenote.mynote.util.ParamsCheckUtils;
import com.litenote.mynote.util.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 *
 */
@Service
@Slf4j
public class UserInfoService extends ServiceImpl<UserInfoMapper, UserInfo> {

    @Autowired
    UserRoleService userRoleService;

    @Autowired
    IdWorker idWorker;

    @Autowired
    RedisUtils redisUtils;

    /**
     * 小程序 appId
     */
    @Value("${wx.appId}")
    private String appId;

    /**
     * 小程序 appSecret
     */
    @Value("${wx.secret}")
    private String secret;

    /**
     * 授权类型，此处只需填写 authorization_code
     */
    @Value("${wx.grantType}")
    private String grantType;

    private static String authUrl = "https://api.weixin.qq.com/sns/jscode2session?appid={appid}&secret={secret}&js_code={js_code}&grant_type={grant_type}";

    /**
     * @Description: 获取用户session_key
     * @Param: authQo 请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    public AuthVo code2Session(AuthQo authQo) {
        boolean c = ParamsCheckUtils.hasEmpty(appId, authQo.getJs_code(), secret, grantType);
        if (Boolean.FALSE.equals(c)) {
            log.info("请求参数缺失，当前入参：{}", JSONObject.toJSONString(authQo));
            throw new BizException(Bizerror.PARAMS_ERROR);
        }
        Map<String, Object> params = new HashMap<>(4);
        params.put("appid", appId);
        params.put("secret", secret);
        params.put("js_code", authQo.getJs_code());
        params.put("grant_type", grantType);
        String resultJson = HttpUtils.doGetReq(authUrl, params);
        if (StrUtil.isEmpty(resultJson)) {
            throw new BizException(Bizerror.REQ_BAD);
        }
        AuthVo authVo = JSONObject.parseObject(resultJson, AuthVo.class);
        if (ObjectUtil.isNull(authVo) || StrUtil.isEmpty(authVo.getOpenid())) {
            log.info("拉取微信用户信息异常，当前用户信息：{}", JSONObject.toJSONString(authVo));
            throw new BizException(Bizerror.USER_INFO_ERROR);
        }
        boolean set = redisUtils.set(CommonConstant.USER_LOGIN_KEY + authVo.getSession_key().trim(), authVo.getSession_key(), CommonConstant.EXPIRE_TIME);
        if (Boolean.FALSE.equals(set)) {
            log.info("storage failed,authVo:{}", JSONObject.toJSONString(authVo));
        }
        //登录
        bizLogin(authQo, authVo);
        return authVo;
    }

    /**
     * @Description: 登录
     * @Param: loginQo 登录请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @Transactional(rollbackFor = Exception.class)
    public void bizLogin(AuthQo authQo, AuthVo authVo) {
        authVo.setCode(0);
        Optional<UserInfo> userInfo = this.lambdaQuery()
                .eq(ObjectUtil.isNotNull(authVo.getUnionid()), UserInfo::getUnionId, authVo.getUnionid())
                .eq(UserInfo::getOpenId, authVo.getOpenid())
                .last("limit 1")
                .oneOpt();
        if (userInfo.isPresent()) {
            UserRole userRole = userRoleService.getBaseMapper().selectOne(Wrappers.<UserRole>query()
                    .lambda()
                    .eq(UserRole::getUserId, userInfo.get().getId()));
            if (ObjectUtil.isNull(userRole)) {
                UserRole ur = new UserRole();
                ur.setUserId(userInfo.get().getId());
                ur.setRole(0);
                userRoleService.save(ur);
                authVo.setIsMaster(ur.getRole());
            }
            authVo.setIsMaster(userRole.getRole());
            authVo.setUserId(userInfo.get().getId());
        } else {
            UserInfo newUserInfo = new UserInfo();
            BeanUtils.copyProperties(authQo, newUserInfo);
            newUserInfo.setUnionId(authVo.getUnionid());
            newUserInfo.setOpenId(authVo.getOpenid());
            newUserInfo.setAvatar(authQo.getAvatarUrl());
            newUserInfo.setId(idWorker.nextId(null).longValue());
            this.saveOrUpdate(newUserInfo);
            UserRole userRole = new UserRole();
            userRole.setUserId(newUserInfo.getId());
            userRole.setRole(0);
            //TODO 获取手机号
            userRoleService.saveOrUpdate(userRole);
            authVo.setUserId(newUserInfo.getId());
            authVo.setIsMaster(userRole.getRole());
            if (ObjectUtil.isNull(authVo.getOpenid())) {
                authVo.setCode(-1);
            }
        }
    }
}




