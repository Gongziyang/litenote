package com.litenote.mynote.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.domain.ApplyRecord;
import com.litenote.mynote.domain.CommentInfo;
import com.litenote.mynote.domain.NoteDetail;
import com.litenote.mynote.mapper.NoteDetailMapper;
import com.litenote.mynote.vo.NoteDetailVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 *
 */
@Service
public class NoteDetailService extends ServiceImpl<NoteDetailMapper, NoteDetail> {


    @Autowired
    PictureService pictureService;

    @Autowired
    CommentInfoService commentInfoService;

    @Autowired
    ApplyRecordService applyRecordService;

    /**
     * @Description: 查看笔记详情
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    public NoteDetailVo noteDetail(Long noteId, Long userId) {
        Optional<NoteDetail> noteDetail = this.lambdaQuery().eq(NoteDetail::getNoteId, noteId).orderByDesc(NoteDetail::getCreateTime).oneOpt();
        if (noteDetail.isPresent()) {
            NoteDetailVo noteDetailVo = new NoteDetailVo();
            BeanUtils.copyProperties(noteDetail.get(), noteDetailVo);
            noteDetailVo.setNoteId(String.valueOf(noteDetail.get().getNoteId()));
            //互动数据
            getInteractionData(noteDetailVo, noteId, userId);
            return noteDetailVo;
        }
        return null;
    }

    /**
     * @Description: 获取上一条，下一条
     * @Param: noteId 笔记ID
     * @Param: index 0上一条 1下一条
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    public NoteDetailVo getPreOrNext(Long noteId, Integer index, Long userId) {
        Optional<NoteDetail> noteDetail = this.lambdaQuery().eq(NoteDetail::getNoteId, noteId).last("limit 1").oneOpt();
        if (noteDetail.isPresent()) {
            Optional<NoteDetail> noteDetail1 = null;
            if (Integer.valueOf(0).equals(index)) {
                noteDetail1 = this.lambdaQuery()
                        .ne(NoteDetail::getNoteId, noteDetail.get().getNoteId())
                        .ge(NoteDetail::getNoteTime, noteDetail.get().getNoteTime())
                        .orderByAsc(NoteDetail::getNoteTime)
                        .last("limit 1")
                        .oneOpt();
            } else {
                noteDetail1 = this.lambdaQuery()
                        .ne(NoteDetail::getNoteId, noteDetail.get().getNoteId())
                        .le(NoteDetail::getNoteTime, noteDetail.get().getNoteTime())
                        .orderByDesc(NoteDetail::getNoteTime)
                        .last("limit 1")
                        .oneOpt();
            }
            if (noteDetail1.isPresent()) {
                NoteDetailVo noteDetailVo = new NoteDetailVo();
                BeanUtils.copyProperties(noteDetail1.get(), noteDetailVo);
                noteDetailVo.setNoteId(String.valueOf(noteDetail1.get().getNoteId()));
                getInteractionData(noteDetailVo, noteDetail1.get().getNoteId(), userId);
                return noteDetailVo;
            }
        }
        return null;
    }


    /**
     * 获取互动数据
     */
    public void getInteractionData(NoteDetailVo noteDetailVo, Long noteId, Long userId) {
        //评论数量
        noteDetailVo.setCommentNumber(commentInfoService.count(Wrappers.<CommentInfo>query().lambda().eq(CommentInfo::getNoteId, noteId)));
        //点赞数量
        noteDetailVo.setCommentNumber(applyRecordService.lambdaQuery().eq(ApplyRecord::getNoteId, noteId).count());
        noteDetailVo.setIsClick(0);
        if (ObjectUtil.isNotNull(userId)) {
            //是否已经点赞
            Optional<ApplyRecord> applyRecord = applyRecordService.lambdaQuery()
                    .eq(ApplyRecord::getNoteId, noteId)
                    .eq(ApplyRecord::getUserId, userId)
                    .oneOpt();
            if (applyRecord.isPresent()) {
                noteDetailVo.setIsClick(1);
            }
        }
    }
}




