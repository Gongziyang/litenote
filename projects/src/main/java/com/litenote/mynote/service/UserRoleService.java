package com.litenote.mynote.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.domain.UserRole;
import com.litenote.mynote.mapper.UserRoleMapper;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class UserRoleService extends ServiceImpl<UserRoleMapper, UserRole> {

}




