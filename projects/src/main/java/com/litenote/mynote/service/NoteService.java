package com.litenote.mynote.service;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.domain.*;
import com.litenote.mynote.enums.Bizerror;
import com.litenote.mynote.exceptions.BizException;
import com.litenote.mynote.mapper.NoteMapper;
import com.litenote.mynote.qo.*;
import com.litenote.mynote.util.IdWorker;
import com.litenote.mynote.vo.ApplyVo;
import com.litenote.mynote.vo.BaseVo;
import com.litenote.mynote.vo.CommentVo;
import com.litenote.mynote.vo.NoteVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 */
@Service
public class NoteService extends ServiceImpl<NoteMapper, Note> {

    @Autowired
    IdWorker idWorker;

    @Autowired
    NoteDetailService noteDetailService;

    @Autowired
    CommentInfoService commentInfoService;

    @Autowired
    PictureService pictureService;

    @Autowired
    ApplyRecordService applyRecordService;

    @Autowired
    UserInfoService userInfoService;

    /**
     * @Description: 分页查询笔记列表
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    public BaseVo<List<NoteVo>> notesByPage(NotesQo noteQo) {
        String noteName = null;
        Integer pageNo = 1;
        Integer pageSize = 15;
        if (ObjectUtil.isNotNull(noteQo) && ObjectUtil.isNotNull(noteQo.getPageNo()) && ObjectUtil.isNotNull(noteQo.getPageSize())) {
            pageNo = noteQo.getPageNo();
            pageSize = noteQo.getPageSize();
            if (StrUtil.isBlank(noteQo.getNoteName())) {
                noteName = noteQo.getNoteName();
            }
        }
        Page<Note> page = new Page<>(pageNo, pageSize);
        Page<Note> result = this.lambdaQuery().eq(ObjectUtil.isNotNull(noteName), Note::getName, noteName).orderByDesc(Note::getNoteTime).page(page);
        if (ObjectUtil.isNotNull(result)) {
            List<NoteVo> collect = result.getRecords().stream().map(note -> {
                NoteVo v = new NoteVo();
                BeanUtils.copyProperties(note, v);
                v.setId(String.valueOf(note.getId()));
                Optional<Picture> picture = pictureService.lambdaQuery().eq(Picture::getNoteId, note.getId()).last("limit 1").oneOpt();
                if (picture.isPresent()) {
                    ArrayList<String> picUrls = new ArrayList<>(1);
                    picUrls.add(picture.get().getPictureUrl());
                    v.setPicUrls(picUrls);
                }
                return v;
            }).collect(Collectors.toList());
            BaseVo<List<NoteVo>> baseVo = new BaseVo();
            baseVo.setPageNo(pageNo);
            baseVo.setPageSize(pageSize);
            baseVo.setPages(result.getPages());
            baseVo.setData(collect);
            baseVo.setTotal(getBaseMapper().getTotal());
            return baseVo;
        }
        return null;
    }

    /**
     * @Description: 新增笔记
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @Transactional(rollbackFor = Exception.class)
    public BaseVo saveNote(SaveNoteQo saveNoteQo) {
        boolean operation = Boolean.TRUE;
        Note note = new Note();
        BeanUtils.copyProperties(saveNoteQo, note);
        //编辑操作
        if (ObjectUtil.isNotNull(saveNoteQo.getId())) {
            Optional<Note> opt = this.lambdaQuery().ne(Note::getId, saveNoteQo.getId()).eq(Note::getName, saveNoteQo.getNoteName()).oneOpt();
            if (opt.isPresent()) {
                throw new BizException(Bizerror.NOTE_NAME_EXSITZ);
            }
            note.setUpdateTime(new Date());
            note.setTimestamp(note.getNoteTime().getTime());
            String brief = saveNoteQo.getContextBrief().length() > 50 ? saveNoteQo.getContextBrief().substring(0, 50) : saveNoteQo.getContextBrief();
            note.setContextBrief(brief);
            operation = this.updateById(note);
            Optional<NoteDetail> noteDetailOpt = noteDetailService.lambdaQuery().eq(NoteDetail::getNoteId, saveNoteQo.getId()).oneOpt();
            if (noteDetailOpt.isPresent()) {
                noteDetailOpt.get().setContext(saveNoteQo.getContextBrief());
                noteDetailOpt.get().setUpdateTime(new Date());
                noteDetailOpt.get().setNoteTime(saveNoteQo.getNoteTime());
                noteDetailOpt.get().setTimestamp(noteDetailOpt.get().getNoteTime().getTime());
                operation = noteDetailService.updateById(noteDetailOpt.get());
            } else {
                NoteDetail noteDetail = new NoteDetail();
                noteDetail.setNoteId(saveNoteQo.getId());
                noteDetail.setContext(saveNoteQo.getContextBrief());
                noteDetail.setCreateTime(new Date());
                noteDetail.setUpdateTime(new Date());
                if (ObjectUtil.isNull(saveNoteQo.getNoteTime())) {
                    noteDetail.setNoteTime(noteDetail.getCreateTime());
                } else {
                    noteDetail.setNoteTime(saveNoteQo.getNoteTime());
                }
                noteDetail.setTimestamp(noteDetail.getNoteTime().getTime());
                operation = noteDetailService.save(noteDetail);
            }
        } else {
            Note exsitNote = this.getBaseMapper().selectOne(Wrappers.<Note>query().lambda().eq(Note::getName, saveNoteQo.getNoteName()));
            if (ObjectUtil.isNotNull(exsitNote)) {
                throw new BizException(Bizerror.NOTE_NAME_EXSITZ);
            }
            note.setCreateTime(new Date());
            note.setUpdateTime(new Date());
            String brief = saveNoteQo.getContextBrief().length() > 50 ? saveNoteQo.getContextBrief().substring(0, 50) : saveNoteQo.getContextBrief();
            note.setContextBrief(brief);
            note.setId(idWorker.nextId(null).longValue());
            if (ObjectUtil.isNull(saveNoteQo.getNoteName())) {
                note.setNoteTime(note.getCreateTime());
            }
            note.setTimestamp(note.getNoteTime().getTime());
            operation = this.save(note);

            NoteDetail noteDetail = new NoteDetail();
            noteDetail.setNoteId(note.getId());
            noteDetail.setContext(saveNoteQo.getContextBrief());
            noteDetail.setCreateTime(new Date());
            noteDetail.setUpdateTime(new Date());
            if (ObjectUtil.isNull(saveNoteQo.getNoteTime())) {
                noteDetail.setNoteTime(noteDetail.getCreateTime());
            } else {
                noteDetail.setNoteTime(saveNoteQo.getNoteTime());
            }
            noteDetail.setTimestamp(noteDetail.getNoteTime().getTime());
            operation = noteDetailService.save(noteDetail);
        }
        BaseVo baseVo = new BaseVo();
        baseVo.setCode(0);
        baseVo.setExtendVal(note.getId());
        if (Boolean.FALSE.equals(operation)) {
            baseVo.setCode(-1);
        }
        return baseVo;
    }

    /**
     * @Description: 删除笔记
     * @Param: noteQo 笔记列表请求体
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    @Transactional(rollbackFor = Exception.class)
    public BaseVo deleteNote(Long noteId, Long userId) {
        Integer userRole = userInfoService.getBaseMapper().getUserRole(userId);
        if (ObjectUtil.isNotNull(userRole)) {
            if (Integer.valueOf(0).equals(userRole)) {
                throw new BizException(Bizerror.NO_PERMISSION);
            }
            boolean affect = Boolean.TRUE;
            this.getBaseMapper().deleteById(noteId);
            noteDetailService.getBaseMapper().delete(Wrappers.<NoteDetail>query().lambda().eq(NoteDetail::getNoteId, noteId));
            pictureService.getBaseMapper().delete(Wrappers.<Picture>query().lambda().eq(Picture::getNoteId, noteId));
            BaseVo baseVo = new BaseVo();
            baseVo.setCode(0);
            return baseVo;
        }
        return null;
    }

    /**
     * 点赞
     *
     * @param noteId
     */
    public void applyNote(ApplyQo applyQo) {
        ApplyRecord applyRecord = new ApplyRecord();
        BeanUtils.copyProperties(applyQo, applyRecord);
        applyRecord.setCreateTime(new Date());
        applyRecord.setUpdateTime(new Date());
        applyRecordService.saveOrUpdate(applyRecord);
    }

    /**
     * 取消点赞
     *
     * @param noteId
     */
    public void unApplyNote(ApplyQo applyQo) {
        applyRecordService.getBaseMapper()
                .delete(Wrappers.<ApplyRecord>query()
                        .lambda()
                        .eq(ApplyRecord::getNoteId, applyQo.getNoteId())
                        .eq(ApplyRecord::getUserId, applyQo.getUserId()));
    }


    /**
     * 评论
     *
     * @param commentQo
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public BaseVo commentNote(CommentQo commentQo) {
        BaseVo baseVo = new BaseVo();
        baseVo.setCode(-1);
        if (ObjectUtil.isNotNull(commentQo.getNoteId())) {
            Optional<Note> note = this.lambdaQuery().eq(Note::getId, commentQo.getNoteId()).oneOpt();
            if (note.isPresent()) {
                CommentInfo commentInfo = new CommentInfo();
                BeanUtils.copyProperties(commentQo, commentInfo);
                commentInfo.setCreateTime(new Date());
                commentInfo.setUpdateTime(new Date());
                baseVo.setCreateTime(commentInfo.getCreateTime());
                commentInfo.setId(idWorker.nextId(null).longValue());
                commentInfo.setTimestamp(commentInfo.getCreateTime().getTime());
                boolean save = commentInfoService.save(commentInfo);
                if (Boolean.FALSE.equals(save)) {
                    baseVo.setCode(-1);
                    return baseVo;
                }
                baseVo.setCode(0);
                baseVo.setExtendVal(commentInfo.getId());
            }
        }
        return baseVo;
    }

    /**
     * 删除评论
     *
     * @param commentId
     * @param userId
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public BaseVo deleteComment(Long commentId, Long userId) {
        BaseVo baseVo = new BaseVo();
        int delete = commentInfoService.getBaseMapper().delete(Wrappers.<CommentInfo>query().lambda().eq(CommentInfo::getId, commentId).eq(CommentInfo::getUserId, userId));
        baseVo.setCode(0);
        return baseVo;
    }

    /**
     * 获取点赞用户列表
     *
     * @param noteId
     * @return
     */
    public BaseVo<List<ApplyRecord>> queryApplyUsers(QueryApplyQo queryApplyQo) {
        Page<ApplyVo> page = new Page<>(queryApplyQo.getPageNo(), queryApplyQo.getPageSize());
        List<ApplyVo> commentVos = applyRecordService.getBaseMapper().queryApplyUsers(page, queryApplyQo.getNoteId());
        BaseVo baseVo = new BaseVo();
        baseVo.setPages(page.getPages());
        baseVo.setTotal(page.getTotal());
        baseVo.setData(commentVos);
        return baseVo;
    }
}




