package com.litenote.mynote.service;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.litenote.mynote.domain.*;
import com.litenote.mynote.mapper.*;
import com.litenote.mynote.qo.QueryCommentQo;
import com.litenote.mynote.vo.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.TreeSet;

/**
 *
 */
@Service
public class CommentInfoService extends ServiceImpl<CommentInfoMapper, CommentInfo> {

    /**
     * @Description: 查看笔记评论列表
     * @Param: noteId 笔记Id
     * @Author: yangYiFeng
     * @UpdateUser: yangYiFeng
     * @Return: List<NoteVo>
     * @date: 2022/2/11 23:23
     * @version: 1.0
     * @status: undone
     */
    public BaseVo<List<CommentVo>> queryCommentList(QueryCommentQo queryCommentQo) {
        if (ObjectUtil.isNull(queryCommentQo.getPageNo()) || ObjectUtil.isNull(queryCommentQo.getPageSize())) {
            queryCommentQo.setPageNo(1);
            queryCommentQo.setPageSize(15);
        }
        Page<CommentVo> page = new Page<>(queryCommentQo.getPageNo(), queryCommentQo.getPageSize());
        BaseVo<List<CommentVo>> baseVo = new BaseVo<>();
        baseVo.setData(this.getBaseMapper().getCommentById(page, queryCommentQo.getNoteId()));
        baseVo.setTotal(page.getTotal());
        baseVo.setPages(page.getPages());
        return baseVo;
    }

}




